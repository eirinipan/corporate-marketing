## Request support for contributed article

<!-- Requester please fill in all relevant sections above the solid line. 
Some details may not be applicable. NOTE: Lead time requirements = 1-2 working week for abstract and outline, 2-3 weeks from accepted work date for draft -->

- **Topic:**
- **Initiative/campaign:** <!-- ex: Operation Octocat/Kubecon/remote work -->
- **CTA and UTM link (if applicable):**
- **Submission deadline (date, time, timezone):**
- **Is this for a specific publication/outlet?:** <!-- Yes/No -->
- **If so, which one? (include publication link):**
- **Estimated word count:** 
- **Who is the author/subject matter expert (SME)? Please include name + @gitlab handle, job title, and area of expertise:** 
- **High-res photo of author required?:** <!-- Yes/No; if Yes, please attach image file -->
- **Bio of author/subject matter expert:** 

### Proposal or Prompt
*Please be as specific as possible, include any helpful links or resources, and try to answer the 5Ws + 1H:*
- *Who: Who will be affected or will benefit from this article?*
- *What: What is the article about? What is the problem or solution described in the article?*
- *When (if applicable): When did it happen? When was the last update? When will the effects be felt? When can readers expect to learn more?*
- *Where: Where is this taking/where did it take place? Where should readers go to learn more?*
- *Why: Why did this happen? Why should readers care?*
- *How: How was this handled? If there was a problem, how was it solved? How will it impact the readers or the subject matter?*

[add details here]

## TO DO

- [ ] Requestor completes all details of the form
- [ ] SME approves
- [ ] Work is accepted, writer is assigned (if not written by SME), deadline added, and issue moves to `mktg-status::wip` @JMLeslie
- [ ] Writer interviews SME (if applicable)
- [ ] First draft shared
- [ ] SME reviews draft
- [ ] PR agency reviews draft 
- [ ] Final draft is shared with SME
- [ ] SME approves
- [ ] Article sent for publication 


/label  ~"corporate marketing" ~"Contributed Articles" ~"PR" ~"Corp Comms" ~"mktg-status::plan" 

/assign @JMLeslie

/confidential
