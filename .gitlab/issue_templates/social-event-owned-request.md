#### Request for corporate marketing led GitLab owned event related organic (non-paid) social promotion. *For GitLab owned events ONLY*
<!-- **** This issue template is for the corporate events marketing team to use in aiding with a social campaign. If the event is not attached to corporate marketing, we won't be able to support on our brand social channels. -->

#### 📬 STEP 1: For Requester <!-- Requester please fill in all **[sections]** above the solid line for STEP 1. Please do not open the issue until you're ready to answer the following questions. If the social team still needs more info, they'll ask in the comments. Note that these questions are critical for understanding your request. -->

##### Which part of the GitLab owned event campaign is this issue for?
- [ ] CFP (pre-event)
- [ ] User registration (pre-event)
- [ ] Engagement, coverage, user registration (during event)
- [ ] Post event promo

**please name this issue Social [ANSWER ABOVE] Event Name** e.g _Social CFP Commit 2022_ or _Social Reg Commit 2022_

##### Pertinent date(s):<!-- Please include dates like ticket sale launches, Speaker dates, actual event date(s), etc. -->

`add date(s) here`

##### Add appropriate link(s) for user journey; Consider required creative. Please be sure consider UTM tracking of owned links.
- [ ] `add description` - `add link here`
- [ ] `add description` - `add link here`
- [ ] `add description` - `add link here`

##### Creative assets are required; sometimes are included with the URL to promote via a social card 
_Creative elements, like images or videos, are required for sharing on organic social channels. [Check links using this social card validator](https://cards-dev.twitter.com/validator). If there are no creative assets, including no opengraph card attached to the URL link we'll be promoting, social cannot fulfill this request._

* [ ]  There is an existing social card/opengraph attached to the link(s)
* [ ]  Existing assets are here: `insert link to related issue or repo for creative`
* [ ]  I require new custom assets for social (requires working with brand design team)
* [ ]  I'm not sure what I require (the social team will review with you in the comments below) 

##### Are there relevant hashtags or profiles to include in social copy?

`[add hashtag(s) here]`

##### Are there handles/profiles you'd like the social media team to follow/monitor before/during your event? <!-- These specifics help the social team schedule engagement coverage during the event by monitoring user activity. -->

`[add links to handles/profiles here]`

##### Are you considering or will be adding paid social advertising for this campaign? <!-- Note that paid social advertising is managed by the digital marketing programs team and will require a separate issue -->

* [ ]  Yes, I'm requesting paid social advertising `[add link to paid social issue]`
* [ ]  No, I am not requesting paid social advertising

##### Anything else we should know or could be helpful to know?

`[add additional thoughts here]`

##### 📝 Confirm you've done these actions
* [ ]  I've filled out the above with the most information I have available
* [ ]  I've linked all of the related issues and epics to this issue
-----

### 📝 STEP 2: Social Team: To-Do's
<!-- Be sure there is enough detail with the info above. If an area is not pertinet (e.g. live coverage isn't necessary), please use a strikethrough <s> to <s> to cross out the text.-->  

* [ ]  Review details above and begin drafting brand posts
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please open a design issue, tag requester, and link as a related issue here.

### ✍️ STEP 3: All: Drafts, suggestions, and schedule 
*Please work on these items in the comments below.*

### 🗓 STEP 4: Social Team: Scheduled posts

* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Advocacy: Be sure to as 1 post / 2-3 copy suggestions per channel, to support this initiative
* [ ]  This issue requires live community management coverage. `[insert social team member handle]` will be actively scheduled during `[window of time]`.

------

### 📭 Social Team: Once the posts available to add to Sprout are scheduled and live coverage is completed, you may close this issue.

/label ~Events ~"Social Media" ~"Corp Comms" ~"Corporate Marketing"

/assign @social

/milestone %"Organic Social: Triage"
