## Background
This issue will contain information on an internal contest named `event`. 

This issue is marked 🟡confidential🟡 because it may contain financial figures, approval processes, tax information, and team member contact info.

## Timeline
* The contest will be communicated to the team in `tbd`
* The winners will be announced before the end of the `tbd`

## Details
* [x]  How many winners?
   - `winner name`
   - `winner name`
* [x]  List Prizes
   *  `prize`
* [x]  Prize Value?
   *  `$x per person`
* [x]  Decide who covers the tax - Employee or Company?
   *  `Employee`

To-Do
- [ ] @wspillane to add tags for winners to comments and tag `@vlaughlan @Javonna @ybasha` for US Payroll and `@nprecilla @sszepietowska` for non-US payroll











<!-- Please leave the label below on this issue -->
