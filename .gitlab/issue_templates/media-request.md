<!-- To be completed by the Corp Comms team or agency.--> 
*Note: The issue should be created when a media opportunity arises. Share the opportunity in #external-comms for wider visibility and link to any PR tracking issues. For executive comms/remote work, assign to (`@nwoods1`) and/or (`@rromoff`). For global and broad tech initiatives, assign to (`@cweaver1`). For PubSec, security and partners, assign to (`@kpdoespr`). For bylines and contributed content, assign to (`@akulks`).*

## :bulb: Opportunity
*Insert details on the interview, how it came about and if it is related to any ongoing intiatives/launches. Include any assets that may be helpful in preparing for the interview and details on past relationship with the reporter.*
* XX

## :notebook: Briefing Document
*Tag DRI to share briefing materials and include date and time to deliver.*
* [ ] XX

## :microphone2: Interview
*Include next steps on scheduling interview, tag interview coordinator, include EBA as needed.*
* [ ] XX

## :dart: Goals 
*Describe ROI on this opportunity under both thought leadership and awareness subcategories.*
* Visibility
* Thought leadership
* Awareness 

## :mega: Spread the Word!
* [ ] Flag: 'Insert DRI' to notify team when article publishes
* [ ] Slack: 'Insert DRI' to manage
* [ ] Social: (`@wspillane`) to promote

## :rolled_up_newspaper: Coverage
*Complete after interview.*
* Publication, Author: Hyperlinked Title

------

/label ~"Corporate Marketing" ~"Corp Comms" ~"PR" ~"Thought Leadership" ~"Media Relations" ~"Executive Visibility"

/confidential
