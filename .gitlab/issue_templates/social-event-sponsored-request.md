Request for **corporate marketing led 3rd party sponsored event** related organic (non-paid) social promotion. *NOT for GitLab owned events*
<!-- **** This issue template is for the corporate events marketing team to use in aiding with a social campaign. If the event is not attached to corporate marketing, we won't be able to support on our brand social channels. -->

#### 📬 Step 1: For Requester <!-- Requester please fill in all **[sections]** above the solid line for STEP 1. Please do not open the issue until you're ready to answer the following questions. If the social team still needs more info, they'll ask in the comments. Note that these questions are critical for understanding your request. -->

##### Tell us about the event and why GitLab is participating in this event. Please include what message and story we are trying to tell about GitLab.

`insert story synopsis and other details here`

##### Pertinent Event date(s):<!-- Please include dates like ticket sale launches, Speaker dates, actual event date(s), etc. -->

`add date(s) here`

##### Add appropriate link(s) for user journey; Consider required creative. Registration and event links are necessary. Please be sure to check with the event owner for any UTMs needed for their tracking.
- [ ] `add description` - `add link here`
- [ ] `add description` - `add link here`
- [ ] `add description` - `add link here`

##### Creative assets should be provided by the event owner; sometimes are included with the URL to promote via a social card. If there are no creative assets, including no opengraph card attached to the URL link we'll be promoting, social cannot fulfill this request.

**[Check links using this social card validator](https://cards-dev.twitter.com/validator) and then answer the checkboxes below.**

* [ ]  There is an existing social card/opengraph attached to the link(s)
* [ ]  Existing assets are here: `insert link to related issue or repo for creative`
* [ ]  I'm not sure (the social team will review with you in the comments below) 

##### What objectives do you want users to follow? Note: if the cost of a ticket is above $100USD, the social team will not support registration objective.

* [ ]  Pre-Event Sign-Ups/Registrations 
* [ ]  Awareness around GitLab speakers participating in the event - `insert link to speaker related issue`
* [ ]  Awareness and/or Engagement leading up to and/or during the event from users across social media

##### Please list relevant hashtags for the event provided by the event owner.

`add hashtag(s) here`

##### Are there handles/profiles you'd like the social media team to follow/monitor before/during your event? <!-- These specifics help the social team schedule engagement coverage during the event by monitoring user activity. -->

`add links to handles/profiles here`

##### Would you like request team member advocacy in Bambu?
<!-- When considering if you'd like team member advocacy available for this event, don't automatically answer yes. Consider whether or not GitLab stands to benefit from clicks from team member social media profiles, if the event is on brand enough for general public awareness, and if there are real results to be had.-->
* [ ]  No, I'm not request team member posts in Bambu
* [ ]  Yes, I'd like one post with (2) copy suggestions added to Bambu
- `social team will add bambu link here`

##### Are you considering paid social advertising for this campaign? <!-- Note that paid social advertising is managed by the digital marketing programs team and will require a separate issue -->
* [ ]  Yes, I'm requesting paid social advertising `[add link to paid social issue]`
* [ ]  No, I am not requesting paid social advertising

##### Anything else we should know or could be helpful to know?

`[add additional thoughts here]`

##### 📝 Confirm you've done these actions
* [ ]  I've filled out the above with the most information I have available
* [ ]  I've linked all of the related issues and epics to this issue

-----

### 📝 Step 2: Social Team: To-Do's
<!-- Be sure there is enough detail with the info above. If an area is not pertinet (e.g. live coverage isn't necessary), please use a strikethrough <s> to <s> to cross out the text.-->  

* [ ]  Review details above and begin drafting posts
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please open a design issue, tag requester, and link as a related issue here.

### ✍️ Step 3: All: Drafts, suggestions, and schedule 
*Any discussion items should be worked out in the comments below*

### 🗓 Step 4: Social Team: Scheduled posts

* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Advocacy Request: if the requester asked for Bambu posts, please draft them and publish when appropriate. Once published, add the Bambu link to the section above and notify the requestor so that they may tell the rest of the team in Slack.
* [ ]  This campaign requires live community management coverage. `[insert social team member handle]` will be actively scheduled during `[window of time]`.

-----

### 📭 Social Team: Once the posts available to add to Sprout are scheduled and live coverage is completed, you may close this issue.

/label ~Events ~"Social Media" ~"Corp Comms" ~"Corporate Marketing"

/assign @social

/milestone %"Organic Social: Triage"
